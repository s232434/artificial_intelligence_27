#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 17:25:31 2024

@author: piripuz
"""
import numpy as np
import array
import ctypes
import time


class Player():
    """General class for bots, Each bot can be built as a subclass of
    this.

    """

    def __init__(self, name="Standard bot"):
        # Opens and loads the C file that does the move
        self.cMove = ctypes.CDLL('move/move.so')
        self.cMove.move.argtypes = (np.ctypeslib.ndpointer(dtype=np.int32, ndim=2, flags='C_CONTIGUOUS'), 
                                  np.ctypeslib.ndpointer(dtype=np.int32, ndim=1, flags='C_CONTIGUOUS'),
                                  ctypes.c_int, ctypes.c_int)
        self.name = name

    def attach(self, pl):
        """Function which makes the bot aware of the game it's attached
        to

        """
        self.player = pl

    def move(self, table, dep):
        """Standard move: do a casual move on the board, and reset the
        counters of simulated moves

        """
        self.moves = 0
        return np.random.randint(len(table))

    def _sim_move(self, table, dep, i, pl):
        """Simulates the move, by calling the C function that does so.

        It modifies the arrays in place, and increases the value of
        the counter self.moves

        """

        self.moves = self.moves + 1
        return self.cMove.move(table, dep, pl, i)


class MiniMax(Player):
    """Extends the standard Player class implementing the minimax
    algorithm. The depth is specified in a parameter when creating the
    bot

    """

    def __init__(self, name="MiniMax Bot", depth=6, utility="deps"):
        self.depth = depth
        self.utility = utility
        super(MiniMax, self).__init__(name=name)

    def move(self, table, dep):
        """Function called once for each move.

        Given a table, it returns the best move according to a given
        algorithm, for the player the bot is attached to.

        """

        # Here all the quantities that are initialized once per move
        # are initialized.

        self.moves = 0
        start_time = time.time()
        points = np.array([0] * 6)
        table = table.astype("int32")
        dep = dep.astype("int32")
        for i in range(6):

            # If the move is forbidden, we avoid it by setting an
            # extremely unconvenient weight. There's no risk of having
            # an empty table, since in that case the function move
            # will not be called.

            if (table[self.player, i] == 0):
                points[i] = -1000
                continue

            # For passing the parameter to the move simulation
            # function, we create a copy of theme that can be handled
            # (and modified in place) by the C language.

            table_new = np.ascontiguousarray(table).copy()
            dep_new = np.ascontiguousarray(dep).copy()

            # If the function returns false, the current player has
            # another turn: the move function will thus call max
            # instead of min, and will add one layer of
            # depth. Otherwise, the table will be passed to the min
            # function, which acts as the opponent.

            if self._sim_move(table_new, dep_new, i, self.player):
                # Next move will be the opponent's
                points[i] = self._min(table_new, dep_new, self.depth)
            else:
                # Player has another move
                points[i] = self._max(table_new, dep_new, self.depth)

        # The program is now finished: we compute the time taken to
        # move and print some statistics.

        delta_t = time.time() - start_time

        print(points)
        print(f"Simulated {self.moves} moves, in {delta_t:.2} seconds")

        return np.random.choice(np.flatnonzero(points == points.max()))

    def _max(self, table, dep, num):
        """Function that acts as the player, computing the best move
        to be done and returning only the profit that move would
        bring.

        """

        # If the maximum depth is reached, or if the game is
        # terminated, the utility function is evaluated.

        if (num == 0 or max(table[self.player]) == 0 or max(dep) > 18):
            return self._utility(table, dep)
        else:
            points = array.array('f', [0] * 6)
            for i in range(6):

                # As in the move function, a check is made to see whether a
                # move is permitted. We decided to put an extreme
                # value rather than dinamically allocating the array
                # for performance reasons.

                if (table[self.player, i] == 0):
                    points[i] = -1000
                    continue

                # Like in the move method, arrays are duplicated and
                # passed to the relevant functions.

                table_new = np.ascontiguousarray(table).copy()
                dep_new = np.ascontiguousarray(dep).copy()

                # Again, we check wether another move is given to the
                # player

                if self._sim_move(table_new, dep_new, i, self.player):
                    # Next move is the opponent's: _min function is
                    # called, and depth is reduced
                    points[i] = self._min(table_new, dep_new, num - 1)
                else:
                    # The player repeats the move: the depth is in
                    # this case not reduced
                    points[i] = self._max(table_new, dep_new, num - 1)

            return max(points)

    def _min(self, table, dep, num):
        """Function that acts as the opponent, computing the best move
        it could do and returning only the profit that move would
        bring.

        """
        # This function mirrors exactly the behavior of the max
        # function, except for this one returning the minimum value
        # obtainable. For more detailed comments see the _max method.

        # Check if the game already finished
        if (num == 0 or max(table[1 - self.player]) == 0 or max(dep) > 18):
            return self._utility(table, dep)
        else:
            points = array.array('f', [0] * 6)
            for i in range(6):

                # Check if move is allowed

                if (table[1 - self.player, i] == 0):
                    points[i] = 1000
                    continue

                # Game state is duplicated

                table_new = np.ascontiguousarray(table).copy()
                dep_new = np.ascontiguousarray(dep).copy()

                if self._sim_move(table_new, dep_new, i, 1 - self.player):
                    # Next move is the opponent's
                    points[i] = self._max(table_new, dep_new, num - 1)
                else:
                    # Player can repeat the move
                    points[i] = self._min(table_new, dep_new, num - 1)
            return min(points)

    def _utility(self, table, dep):
        if (dep[self.player] > 18 or max(table[1-self.player]) == 0):
            return 30
        elif (dep[1 - self.player] > 18 or max(table[self.player]) == 0):
            return -30
        match self.utility:
            case "deps":
                return dep[self.player]-dep[1 - self.player]
            case "complete":
                lam = 0.3
                wtab = 12
                wdep = 1
                if self.player == 0:
                    table = np.flip(table, axis=0)
                    dep = np.flip(dep)
                utility_tab = np.sum(lam ** np.arange(1, 7, 1) * table[1] -\
                    lam ** np.arange(6, 0, -1) * table[0])
                utility_dep = dep[1] - dep[0]
                return wdep * utility_dep + wtab * utility_tab

