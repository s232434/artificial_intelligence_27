#include "move.h"
#include <stdio.h>

int move(int rows[2][6], int deps[2], int initial_pl, int pos) {
  int balls = rows[initial_pl][pos];
  rows[initial_pl][pos] = 0;
  int pl = initial_pl;
  int size = 6;
  while (balls > 0) {
    if (pos == size - 1 && pl == 1) {
    /* if ((pos + pl) % size == 0) { */
      deps[1]++;
      pl = 0;
      pos = size;
    } else if (pos == 0 && pl == 0) {
      deps[0]++;
      pl = 1;
      pos = -1;
    } else {
      if (pl == 1) {
        rows[1][++pos]++;
      } else {
        rows[0][--pos]++;
      }
    }
    balls--;
  }
  if (pl == initial_pl && rows[pl][pos] == 1) {
    deps[pl] += rows[1 - pl][pos] + 1;
    rows[0][pos] = 0;
    rows[1][pos] = 0;
  }
  // Returns 0 if the move has to be repeated
  return !((pos == size || pos == -1) && pl != initial_pl);
}
// int main(int argc, char *argv[]) {

//     // I am getting chars from python so each argument had to be converted to int, then i get my matrix_table,matrix_dep
//     // size and position, then i can make the call to the function
//     char *serialized_table = argv[1];
//     int matrix_table[2][6]; 
//     int row = 0, col = 0;
//     char *token,*endptr;
//     token = strtok(serialized_table, " ");
//     while (token != NULL ) {
//         matrix_table[row][col] = strtol(token, &endptr, 10);
//         if (endptr == token) {
//             break;
//         }
//         col++;
//         if (col >= 6) {
//             row++;
//             col = 0;
//         }
//         token = strtok(NULL, " ");
//     }
//     char *serialized_dep = argv[2];
//       int matrix_dep[2]; 
//       row = 0;
//       col = 0;
//       token = strtok(serialized_dep, " ");
//       while (token != NULL ) {
//           matrix_dep[col] = atoi(token);
//           if (endptr == token) {
//               break;
//           }
//           col++;
        
//           token = strtok(NULL, " ");
//       }
//       int size = atoi(argv[3]);
//       int postion= atoi(argv[4]);
//       move(matrix_table,matrix_dep,size,postion);
//     return 0;
// }
