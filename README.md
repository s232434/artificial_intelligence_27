# Kalah

This repository contains our implementation of the game Kalah and some bots that play it.

The python code is in the root: the bots are written into the players.py file, the backend into the kalah.py and the frontend in the frontend.py. The move directory contains files in C, for the bots to be able to move faster. 


## Installation
The game should work on windows and linux. If it doesn't work on linux, compiling the move file should solve the issue:

gcc -shared -o move/move.so -fPIC move/move.c

## Usage
Just launch the main file:

python main.py