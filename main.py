#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 12:09:04 2024

@author: piripuz
"""

from kalah import Kalah
from player import MiniMax, Player
import numpy as np


if (__name__ == "__main__"):
    game = Kalah(console=True)
    weak_bot = MiniMax(depth=3, name="Standard", utility="deps")
    strong_bot = MiniMax(depth=3, name="Powerful", utility="complete")
    random_bot = Player()
    game.attach(weak_bot)
    game.attach(strong_bot)
    res = 0
    num = 50
    moves = []
    for i in range(50):
        r = game.start(initial_pl=np.random.randint(2))
        moves.append(game.moves)
        print(f"###########################{i}#########################################")
        if r == -1:
            num -= 1
        else:
            res += r
    res = res / num
    print(res)
    print(np.mean(moves))
