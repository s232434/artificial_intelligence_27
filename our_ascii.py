#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 16:55:08 2024

@author: piripuz
"""

def line(length=47, char='#'):
    print(char * length)

def index(length=47):
    exc = (length - 35) // 2
    char = "#"
    print(char*exc + "# 0 ### 1 ### 2 ### 3 ### 4 ### 5 #" + char*exc)

def mid(array, length=47, char='#'):
    exc = (length - 37) // 2
    print(char*(exc+1), end='')
    for i in range(len(array)):
        print(f'  {array[i]}  #', end='')
    print(char*(exc+1))

def dep(array, length=47, char='#'):
    exc = (length - 43) // 2
    print(exc*char + f" {array[0]} ##################################### {array[1]} " + exc*char)