#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 11:14:37 2024

@author: piripuz
"""
import numpy as np
import our_ascii
from frontend import Interface
import sys


class Kalah:
    def __init__(
        self, p0_name="Blue Player", p1_name="Red Player", width=6, console=False
    ):
        self.p = [p0_name, p1_name]
        self.num_bot = 0
        self.bot = [0, 0]
        self.console = console
        self.width = width

    def attach(self, bot):
        if self.num_bot > 1:
            print("Cannot attach more than 2 bots")
            return
        self.bot[self.num_bot] = bot
        self.p[self.num_bot] = bot.name
        bot.attach(self.num_bot)
        self.num_bot += 1

    def start(self, initial_pl=0):
        self.moves = 0
        self.table = np.ones((2, self.width), dtype=int) * 3
        self.dep = np.zeros((2), dtype=int)
        self.curr_player = initial_pl
        self.frontend = Interface(self, self.p[0], self.p[1], self.console)
        while not self.finished():
            self.frontend.print_table(self.table, self.dep, self.curr_player)
            cell = self._get_movement(self.bot[self.curr_player])
            if self._move(cell):
                self.moves += 1
                self.curr_player = 1 - self.curr_player
        self.frontend.print_table(self.table, self.dep, self.curr_player)

        self.dep += np.sum(self.table, axis=1)

        print(f"{self.p[0]} gained " + str(self.dep[0]) + " balls")
        print(f"{self.p[1]} gained " + str(self.dep[1]) + " balls")

        if self.dep[0] > self.dep[1]:
            return 0
        elif self.dep[0] < self.dep[1]:
            return 1
        else:
            return -1

    def _get_movement(self, bot):
        if not bot:
            while True:
                cell = self.frontend.get_movement()
                if cell < 6 and cell >= 0:
                    if self.table[self.curr_player, cell] == 0:
                        print(f"{cell+1} is empty. \nRetrying...")
                        continue
                    break
                else:
                    print(f"{cell} is not a valid cell.\nRetrying...")
            return cell
        return bot.move(self.table, self.dep)

    def _move(self, cell):
        num_ball = self.table[self.curr_player, cell]
        self.table[self.curr_player, cell] = 0
        return self._red(cell, num_ball, self.curr_player)

    def _red(self, cell, num_ball, player):
        if num_ball == 0:
            if (
                cell not in [-1, 6]
                and self.table[player, cell] == 1
                and self.curr_player == player
                and self.table[1 - player, cell] != 0
            ):
                self.dep[self.curr_player] += self.table[1 - player, cell] + 1
                self.table[player, cell] = 0
                self.table[1 - player, cell] = 0
                print(f"{self.p[self.curr_player]} gained some balls!!")
                return True
            elif cell in [-1, 6]:
                print("You have another turn")
                return False
            else:
                print("Move ended")
                return True
        if player == 0:
            if cell != 0:
                target = cell - 1
                self.table[player, target] += 1
                return self._red(target, num_ball - 1, player)
            else:
                self.dep[0] += 1
                return self._red(-1, num_ball - 1, 1)
        if player == 1:
            if cell != 5:
                target = cell + 1
                self.table[player, target] += 1
                return self._red(target, num_ball - 1, player)
            else:
                self.dep[1] += 1
                return self._red(6, num_ball - 1, 0)

    def finished(self):
        return max(self.dep) > 18 or np.min(np.max(self.table, axis=1), axis=0) == 0

    def stop(self):
        print("Ciao")
        sys.exit()
