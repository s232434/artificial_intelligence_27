#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 20:14:28 2024

@author: piripuz
"""

import pygame


class Interface:
    def __init__(self, game, p0_name, p1_name, console):
        self.console = console
        self.game = game
        if console:
            return
        # Set up the screen
        self.SCREEN_WIDTH = 800
        self.SCREEN_HEIGHT = 800
        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        pygame.display.set_caption("Kalah")

        # Define colors
        self.WHITE = (255, 255, 255)
        self.BLUE = (0, 0, 255)
        self.RED = (255, 0, 0)
        self.BLACK = (0, 0, 0)
        
        # Define constants for the board
        self.PIT_RADIUS = 30
        self.PIT_DISTANCE = 80
        self.KALAHA_WIDTH = 100
        self.KALAHA_HEIGHT = 250
        self.PLAYER1_PITS = [(200 + i * self.PIT_DISTANCE, 100) for i in range(6)]
        self.PLAYER2_PITS = [(200 + i * self.PIT_DISTANCE, 300) for i in range(6)]
        self.PLAYER1_KALAHA = (50, 75)
        self.PLAYER2_KALAHA = (650, 75)
        
        pygame.font.init()
        self.font = pygame.font.SysFont(None, 36)
        
        self.p0_name = p0_name
        self.p1_name = p1_name
        
    def print_table(self, table, dep, player):
        if self.console:
            print("    ", end="")
            for i in range(1, len(table[0]) + 1):
                print(f"   {i-1}", end="")
            print()
            print("     |", end="")
            for buca in table[0]:
                print(f" {buca} |", end="")
            print("\n| " + str(dep[0]) + " |", end="")
            print("-" * (4 * len(table[0]) + 1), end="")
            print("| " + str(dep[1]) + " |")
            print("     |", end="")
            for buca in table[1]:
                print(f" {buca} |", end="")
            print("\n")
            return

        self.screen.fill(self.WHITE)
        # Draw player 1 pits and stones
        for i, (pit_position, stones) in enumerate(zip(self.PLAYER1_PITS, table[0,:])):
            self._draw_pit(pit_position, self.BLUE, stones, i+1, upper_row=False)
        # Draw player 2 pits and stones
        for i, (pit_position, stones) in enumerate(zip(self.PLAYER2_PITS, table[1,:])):
            self._draw_pit(pit_position, self.RED, stones, i+1, upper_row=True)
        # Draw player 1 Kalaha
        self._draw_kalaha(self.PLAYER1_KALAHA, self.BLUE,dep[0])
        # Draw player 2 Kalaha
        self._draw_kalaha(self.PLAYER2_KALAHA, self.RED,dep[1])
    
        # Display whose turn it is
        if player == 1:
            text_surface = self.font.render(self.p1_name + "'s Turn", True, self.BLACK)
        else:
            text_surface = self.font.render(self.p0_name + "'s Turn", True, self.BLACK)
        text_rect = text_surface.get_rect(center=(self.SCREEN_WIDTH // 2, 50))
        self.screen.blit(text_surface, text_rect)
        
        pygame.display.flip()
        
    def _draw_pit(self, position, color, stones, pit_index, upper_row):
        pygame.draw.circle(self.screen, color, position, self.PIT_RADIUS)
        text_surface = self.font.render(str(stones), True, self.BLACK)  # Render stones in black color
        text_rect = text_surface.get_rect(center=position)
        self.screen.blit(text_surface, text_rect)
        
        if upper_row:
            # For the upper row, display index above the pit
            index_text_y_offset = -50  
        else:
            # For the lower row, display index below the pit
            index_text_y_offset = 50  
        
        index_text = self.font.render(str(pit_index), True, self.BLACK)
        index_text_rect = index_text.get_rect(center=(position[0], position[1] + index_text_y_offset))
        self.screen.blit(index_text, index_text_rect)
        
    def _draw_kalaha(self, position, color,kalaha_stones):
        pygame.draw.rect(self.screen, color, pygame.Rect(position[0], position[1], self.KALAHA_WIDTH, self.KALAHA_HEIGHT))
        text_surface = self.font.render(str(kalaha_stones), True, self.BLACK)
        text_rect = text_surface.get_rect(center=(position[0] + self.KALAHA_WIDTH // 2, position[1] + self.KALAHA_HEIGHT // 2))
        self.screen.blit(text_surface, text_rect)
        
    def get_movement(self):
        while True:
            if self.console:
                return int(input("Select your move"))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.stop()
                elif event.type == pygame.KEYDOWN:
                    return event.key - pygame.K_1
    def stop(self):
        if self.console:
            return
        pygame.display.quit()
        self.game.stop()
